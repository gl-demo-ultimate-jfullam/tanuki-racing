//spring boot REST endpoints for a calculator
@GetMapping("/add")
public int add(@RequestParam int a, @RequestParam int b) {
    return a + b;
}

@GetMapping("/subtract")  
public int subtract(@RequestParam int a, @RequestParam int b) {
    return a - b;
}

@GetMapping("/multiply")
public int multiply(@RequestParam int a, @RequestParam int b) {
    return a * b; 
}

@GetMapping("/divide")
public int divide(@RequestParam int a, @RequestParam int b) {
    return a / b;
}

@GetMapping("/modulus")
public int modulus(@RequestParam int a, @RequestParam int b) {
    return a % b;
}   